import { type RequestHandler, Router } from 'express';
import { statusRouter } from './routers/status.router';
import { executeRouter } from './routers/execute.router';
import { unexpectedErrorHandler, inputValidationErrorHandler, httpErrorHandler } from './middleware/error';
import { NotFoundError } from '../errors/NotFound.error';

const routers = [statusRouter, executeRouter];
const notFound: RequestHandler = (req, res, next) => {
  next(new NotFoundError());
};

const router = Router();

router.use('/api', routers);

router.use(notFound);
router.use(inputValidationErrorHandler);
router.use(httpErrorHandler);
router.use(unexpectedErrorHandler);

export const appRouter = router;
