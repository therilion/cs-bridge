import { type Controller } from '../../types';
import { sendSuccessResponse } from '../services/response.service';

export const getStatus: Controller = (req, res) => {
  sendSuccessResponse({ res });
};
