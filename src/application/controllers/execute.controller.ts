import { sendSuccessResponse } from '../services/response.service';
import { type Controller } from '../../types';

export const executeCs: Controller = (req, res) => {
  const params = req.body;
  sendSuccessResponse({ res, data: params });
};
