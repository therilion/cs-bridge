import type { ErrorRequestHandler } from 'express';
import { StructError } from 'superstruct';
import { sendFailureResponse } from '../../services/response.service';

export const inputValidationErrorHandler: ErrorRequestHandler = (err, req, res, next) => {
  if (err instanceof StructError) {
    const failures = err.failures();
    const responseFailures = failures.map(({ key, message, type }) => {
      let msg = message;

      if (type === 'never') {
        msg = 'Unexpected field';
      }

      return {
        key,
        message: msg,
      };
    });
    sendFailureResponse({ res, message: 'input validation failed', data: responseFailures });
    return;
  }
  next(err);
};