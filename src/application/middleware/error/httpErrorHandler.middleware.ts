import { type ErrorRequestHandler } from 'express';
import { HttpError } from '../../../errors/HttpError.error';
import { respond } from '../../services/response.service';

export const httpErrorHandler: ErrorRequestHandler = (error, req, res, next) => {
  if (error instanceof HttpError) {
    respond({
      res,
      status: 'failure',
      httpStatus: error.statusCode,
      message: error.message,
      data: error.data,
    });
    return;
  }
  next(error);
};
