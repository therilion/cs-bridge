import { type ErrorRequestHandler } from 'express';
import { sendErrorResponse } from '../../services/response.service';

export const unexpectedErrorHandler: ErrorRequestHandler = (err, req, res, next): void => {
  console.log(err);
  sendErrorResponse({ res, message: err.message, data: err });
};

export * from './httpErrorHandler.middleware';
export * from './inputValidationErrorHandler.middleware';