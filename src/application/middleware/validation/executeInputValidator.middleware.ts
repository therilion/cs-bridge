import { type RequestHandler } from 'express';
import { assert, nonempty, object, optional, record, string, unknown } from 'superstruct';

const ValidParams = object({
  FunctionName: nonempty(string()),
  FunctionParameter: optional(record(string(), unknown())),
});

export const executeCsInputValidation: RequestHandler = (req, res, next) => {
  const params = { ...req.body };
  assert(params, ValidParams);
  next();
};
