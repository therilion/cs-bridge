import { Router } from 'express';
import { executeCs } from '../controllers/execute.controller';
import { executeCsInputValidation } from '../middleware/validation/executeInputValidator.middleware';

const router = Router();

router.post('/execute-cs', executeCsInputValidation, executeCs);

export const executeRouter = router;
