import { type ResponderParams, type Responder, type ResponseService } from '../../types';

export const respond: Responder = ({ res, status, httpStatus, message, data, code }) => {
  res.status(httpStatus).json({
    status,
    message,
    data,
    code,
  });
};

export const sendSuccessResponse: ResponseService = args => {
  const respondParams: ResponderParams = {
    ...args,
    message: args.message ? args.message : 'OK',
    httpStatus: 200,
    status: 'success',
  };

  respond(respondParams);
};

export const sendErrorResponse: ResponseService = args => {
  const respondParams: ResponderParams = {
    ...args,
    message: args.message ? args.message : 'ERROR',
    httpStatus: 500,
    status: 'error',
  };

  respond(respondParams);
};

export const sendFailureResponse: ResponseService = args => {
  const respondParams: ResponderParams = {
    ...args,
    message: args.message ? args.message : 'FAIL',
    httpStatus: 400,
    status: 'failure',
  };

  respond(respondParams);
};
