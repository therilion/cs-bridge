import { type Response, type NextFunction, type Request } from 'express';
import { httpErrorHandler } from '../../../application/middleware/error';
import { NotFoundError } from '../../../errors/NotFound.error';

const getNextMock: () => NextFunction = () => {
  return jest.fn();
};

const getResponseMock: () => { status: jest.Mock; json: jest.Mock } = () => {
  return {
    status: jest.fn().mockReturnThis(),
    json: jest.fn(),
  };
};

describe('httpErrorHandler', () => {
  it('should handle an HttpError instance', () => {
    const error = new NotFoundError();
    const req = {};
    const res = getResponseMock();
    const next = getNextMock();
    httpErrorHandler(error, req as Request, res as unknown as Response, next);

    expect(res.status).toHaveBeenLastCalledWith(error.statusCode);
    expect(next).not.toHaveBeenCalled();
  });

  it('should call to next handler if error does not a HttpError instance', () => {
    const errorMessage = 'test error message';
    const error = new Error(errorMessage);
    const req = {};
    const res = getResponseMock();
    const next = getNextMock();
    httpErrorHandler(error, req as Request, res as unknown as Response, next);
    expect(next).toHaveBeenLastCalledWith(error);
  });
});
