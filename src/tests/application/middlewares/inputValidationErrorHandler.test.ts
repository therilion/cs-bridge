import { type Response, type NextFunction, type Request } from 'express';
import { inputValidationErrorHandler } from '../../../application/middleware/error/inputValidationErrorHandler.middleware';
import { validate, string } from 'superstruct';

const getNextMock: () => NextFunction = () => {
  return jest.fn();
};

const getResponseMock: () => { status: jest.Mock; json: jest.Mock } = () => {
  return {
    status: jest.fn().mockReturnThis(),
    json: jest.fn(),
  };
};

describe('inputValidationErrorHandler', () => {
  it('should handle the struct error', () => {
    const [error] = validate(1, string());
    const req = {};
    const res = getResponseMock();
    const next = getNextMock();
    inputValidationErrorHandler(error, req as Request, res as unknown as Response, next);

    expect(res.status).toHaveBeenLastCalledWith(400);
    expect(next).not.toHaveBeenCalled();
  });

  it('should call to next handler if error does not a StructError', () => {
    const errorMessage = 'test error message';
    const error = new Error(errorMessage);
    const req = {};
    const res = getResponseMock();
    const next = getNextMock();
    inputValidationErrorHandler(error, req as Request, res as unknown as Response, next);
    expect(next).toHaveBeenLastCalledWith(error);
  });
});
