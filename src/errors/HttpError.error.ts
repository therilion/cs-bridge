export abstract class HttpError extends Error {
  public readonly statusCode: number;
  public readonly message: string;
  public readonly data: unknown;

  constructor(message: string, statusCode: number, data: unknown) {
    super(message);
    this.message = message;
    this.statusCode = statusCode;
    this.data = data;
    this.name = 'HttpError';
  }
}
