import { HttpError } from './HttpError.error';

export class NotFoundError extends HttpError {
  constructor(data?: unknown) {
    super('Not found', 404, data);
    this.name = 'NotFoundError';
  }
}
