import { type Response } from 'express';

export type ResponseServiceParams = {
  res: Response;
  message?: string;
  data?: unknown;
  code?: number;
};

export type ResponderParams = {
  res: Response;
  httpStatus: number;
  status: 'error' | 'failure' | 'success';
  message: string;
  data?: unknown;
  code?: number;
};

export type ResponseService = (args: ResponseServiceParams) => void;
export type Responder = (args: ResponderParams) => void;
