import { type NextFunction, type Request, type Response } from 'express';
import { type Logger } from 'winston';

declare module 'express-serve-static-core' {
  interface Request {
    logger: Logger;
  }
}

export type Controller = (req: Request, res: Response, next?: NextFunction) => void;
export type Middleware = (req: Request, res: Response, next: NextFunction) => void;
export type ErrorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => void;

export * from './error.types';
export * from './response.types';
