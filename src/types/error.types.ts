export type HttpError = {
  readonly code: number;
  readonly message: string;
  readonly data: unknown;
};
