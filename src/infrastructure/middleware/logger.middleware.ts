import { v4 as uuid } from 'uuid';
import morgan from 'morgan';
import { type Middleware } from '../../types';
import { logger } from '../logger';

export const loggerMiddleware: Middleware = (req, res, next) => {
  req.logger = logger.child({ requestId: uuid() });

  const morganMiddleware = morgan('tiny', {
    stream: {
      write: message => req.logger.info(message.trim()),
    },
  });
  
  morganMiddleware(req, res, next);
};
