import dotenv from 'dotenv';

dotenv.config();

export const EXPRESS_CONFIG = {
  PORT: Number(process.env.EXPRESS_SERVER_PORT ?? 3000),
};

export const WINSTON_CONFIG = {
  LEVEL: process.env.LOG_LEVEL ?? 'debug',
};
