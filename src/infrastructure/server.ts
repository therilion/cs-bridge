import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import { EXPRESS_CONFIG } from './config';
import { appRouter } from '../application/routes';
import { loggerMiddleware } from './middleware/logger.middleware';

const { PORT } = EXPRESS_CONFIG;

export const startServer = (): void => {
  const app = express();

  app.use(loggerMiddleware);
  app.use(helmet());
  app.use(cors());
  app.use(express.json());
  app.use(appRouter);

  app.listen(PORT, () => {
    console.log(`Listen in port ${PORT}`);
  });
};
