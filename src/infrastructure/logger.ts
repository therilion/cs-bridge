import { createLogger, config, format, transports } from 'winston';
import { v4 as uuid } from 'uuid';
import { WINSTON_CONFIG } from './config';

const { combine, timestamp, prettyPrint } = format;

export const logger = createLogger({
  levels: config.syslog.levels,
  level: WINSTON_CONFIG.LEVEL,
  format: combine(timestamp(), prettyPrint()),
  defaultMeta: {
    instanceId: uuid(),
  },
  transports: [
    new transports.Console(),
  ],
});
